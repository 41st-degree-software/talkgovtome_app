# README #

This is the iOS App, it uses Microsoft's Oxford Project to capture and convert a users voice input into a string.

The app then passes the string on to our API, which in turn returns a text based response.

The app then passes that response to our synth API, which returns an audio file.

The app then plays the audio file, shows the text response and date source.

Other repos

https://bitbucket.org/41st-degree-software/talkgovtomesynth

https://bitbucket.org/41st-degree-software/talkgovtome-api