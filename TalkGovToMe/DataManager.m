//
//  DataManager.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager

+ (NSString *)urlString {
    //return @"http://10.10.10.131/api/public";
    return @"http://45.55.22.87:8080";
}

+ (NSString *)voiceUrlString {
    return @"http://talkgovtomesynth.azurewebsites.net/api/values?text=";
}

+ (void)submitFacebookUser:(APIUser *)user completion:(void (^)(BOOL, NSError *))completion
{
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user",[DataManager urlString]]];
   
    NSDictionary *userDictionary = [user dictionary];
    NSError *jsonError;
    NSData *data = [NSJSONSerialization dataWithJSONObject:userDictionary options:0 error:&jsonError];
    
     NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionDataTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   NSString *dataString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                   
                   if ((error || urlResponse.statusCode != 200) && completion) {
                       completion(false, error);
                   } else if(completion) {
                       completion(true, nil);
                   }
               }];
    [task resume];
}

+ (void)submitInputText:(APIInput *)input completion:(void (^)(APIAnswer *, NSError *))completion {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/input", [DataManager urlString]]];
    
    NSError *jsonError;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[input dictionary] options:0 error:&jsonError];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                   
                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                   
                   if ((error || urlResponse.statusCode != 200) && completion) {
                       completion(false, error);
                   } else {
                       NSError *error;
                       NSString *json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                       NSDictionary *dictionary = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
                       APIAnswer *answer = [APIAnswer answerFromDictionary:dictionary];
                       
                       if (completion) {
                           completion(answer, nil);
                       }
                   }
               }];
    [task resume];
}

+ (void)submitLocation:(APILocation *)location {
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/user/location", [DataManager urlString]]];
    
    NSError *jsonError;
    NSData *data = [NSJSONSerialization dataWithJSONObject:[location dictionary] options:0 error:&jsonError];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"POST";
    request.HTTPBody = data;
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionTask *task =
    [session dataTaskWithRequest:request
               completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                   NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
                   
                   NSString *responseString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
               }];
    [task resume];
}

+ (void)getAudioForText:(NSString *)text completion:(void (^)(NSData *, NSError *))completion {
    NSString *escapedQuery = [text stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLQueryAllowedCharacterSet]];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@", [DataManager voiceUrlString], escapedQuery]];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request addValue:@"audio/wav" forHTTPHeaderField:@"Accept"];
    
    NSURLSessionTask *task =
        [session downloadTaskWithRequest:request
                       completionHandler:^(NSURL *location, NSURLResponse *response, NSError *error) {
                           NSData *data = [NSData dataWithContentsOfURL:location];
                           
                           if (completion) {
                               completion(data, nil);
                           }
                       }];
    [task resume];
}

@end
