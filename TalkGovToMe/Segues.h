//
//  Segues.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Segues : NSObject

+ (NSString *)FacebookLogin;

@end
