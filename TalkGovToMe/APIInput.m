//
//  APIInput.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "APIInput.h"

@implementation APIInput

+ (APIInput *)inputWithText:(NSString *)text facebookId:(NSString *)facebookId
{
    APIInput *input = [APIInput new];
    input.input = text;
    input.facebook_id = facebookId;
    return input;
}

- (NSDictionary *)dictionary {
    return [self dictionaryWithValuesForKeys:@[@"input", @"facebook_id"]];
}

@end
