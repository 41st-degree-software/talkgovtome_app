//
//  LeftBubble.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "LeftBubble.h"

@implementation LeftBubble

- (void)drawRect:(CGRect)rect {
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* color = [UIColor colorWithRed: 0.302 green: 0.698 blue: 0.69 alpha: 1];
    
    CGRect newRect = CGRectMake(7, 0, CGRectGetWidth(self.bounds) - 7.0f, CGRectGetHeight(self.bounds));
    
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect:newRect];
    [color setFill];
    [rectanglePath fill];
    
    //// Rectangle 2 Drawing
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, 6.94, -0.14);
    CGContextRotateCTM(context, 43.96 * M_PI / 180);
    
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 10, 10)];
    [color setFill];
    [rectangle2Path fill];
    
    CGContextRestoreGState(context);
    
    [super drawRect:rect];
}

@end
