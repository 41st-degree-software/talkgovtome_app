//
//  APILocation.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "APILocation.h"

@implementation APILocation

- (NSDictionary *)dictionary {
    return [self dictionaryWithValuesForKeys:@[@"latitude", @"longitude", @"facebook_id"]];
}

@end
