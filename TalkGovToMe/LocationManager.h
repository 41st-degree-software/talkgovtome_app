//
//  LocationManager.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface LocationManager : NSObject

+ (void)setLocation:(CLLocation *)location;
+ (void)setFacebookId:(NSString *)facebookId;

@end
