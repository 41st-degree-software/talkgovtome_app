//
//  OxfordViewController.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpeechSDK/SpeechRecognitionService.h>

@interface OxfordViewController : UIViewController <SpeechRecognitionProtocol>

@end
