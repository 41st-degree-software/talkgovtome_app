//
//  OxfordViewController.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "OxfordViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <AVFoundation/AVFoundation.h>
#import "APIUser.h"
#import "DataManager.h"
#import "LocationManager.h"

@interface OxfordViewController()

@property (strong, nonatomic) MicrophoneRecognitionClient *microphoneClient;

@property (weak, nonatomic) IBOutlet UILabel *spokenTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *dataSetInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *suggestionTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;
@property (weak, nonatomic) IBOutlet UIButton *recordButton;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;

@property (weak, nonatomic) IBOutlet UIView *userBubbe;
@property (weak, nonatomic) IBOutlet UIView *answerBubble;
@property (weak, nonatomic) IBOutlet UIView *suggestionBubble;

@property (nonatomic) BOOL isRecording;
@property (strong, nonatomic) NSString *spokenText;

@property (strong, nonatomic) AVAudioPlayer *player;
@property (strong, nonatomic) NSString *facebookId;

- (IBAction)Record:(id)sender;

@property (weak, nonatomic) IBOutlet UIImageView *meImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *govRightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *meLeftConstraint;
@end

@implementation OxfordViewController

- (void)viewDidLoad {
    // Set the text to empty
//    [self.recordButton setTitle:@"Initialising..." forState:UIControlStateDisabled];
    self.recordButton.enabled = false;
    
    self.spokenText = @" ";
    self.spokenTextLabel.text = [NSString stringWithFormat:@"%@", self.spokenText];
    self.spokenTextLabel.backgroundColor = [UIColor clearColor];
    self.answerTextLabel.text = @" ";
    self.answerTextLabel.backgroundColor = [UIColor clearColor];
    self.suggestionTextLabel.text = @" ";
    self.loadingImage.hidden = YES;
    self.dataSetInfoLabel.text = @" ";
    
    self.userBubbe.alpha = 0.0f;
    self.answerBubble.alpha = 0.0f;
    self.suggestionBubble.alpha = 0.0f;
    
    self.meLeftConstraint.constant = -200;
    self.govRightConstraint.constant = -200;
    
    [self prepare];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.meImage.hidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.meImage.hidden = YES;
}

- (void)prepare
{
    FBSDKAccessToken *token = [FBSDKAccessToken currentAccessToken];
    if ([FBSDKAccessToken currentAccessToken]) {
        [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil]
         startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, NSDictionary *result, NSError *error) {
             if (!error) {
                 APIUser *user = [APIUser userWithFacebookUser:result];
                 user.dob = @"30/06/1980";
                 user.friends = @"100";
                 self.facebookId = user.facebook_id;
                 NSLog(@"fetched user:%@", result);
                 
                 [LocationManager setFacebookId:user.facebook_id];
                 
                 [DataManager submitFacebookUser:user completion:^(BOOL success, NSError *error) {
                     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                         [self initializeRecordingClient];
                         dispatch_async(dispatch_get_main_queue(), ^{
                             self.recordButton.enabled = true;
                         });
                     });
                 }];
             }
         }];
    }
}

-(void)initializeRecordingClient
{
     NSString *language = @"en-us";
     SpeechRecognitionMode recoMode = SpeechRecognitionMode_ShortPhrase;
     NSString *primaryOrSecondaryKey = @"98f539104b4341c2bc53384c2675cc31";
     self.microphoneClient = [SpeechRecognitionServiceFactory createMicrophoneClient:(recoMode)
                                                                        withLanguage:(language)
                                                                             withKey:(primaryOrSecondaryKey)
                                                                        withProtocol:(self)];
}

#pragma mark - Speech Synthesis

- (void)speakText:(NSString *)string completion:(void (^)())completion {
    [DataManager getAudioForText:string completion:^(NSData *data, NSError *error) {
        self.player = [[AVAudioPlayer alloc] initWithData:data fileTypeHint:AVFileTypeWAVE error:&error];
        [self.player play];
        
        if (completion) {
            completion();
        }
    }];
}

#pragma mark - Speech Recognition Protocol

- (void)onMicrophoneStatus:(Boolean)recording {
    if (!recording) {
        [self.microphoneClient endMicAndRecognition];
    }
}

- (void)onPartialResponseReceived:(NSString *)partialResult {
    NSLog(@"%@", partialResult);
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.spokenText = partialResult;
        self.spokenTextLabel.text = [NSString stringWithFormat:@"\"%@\"", self.spokenText];
    });
}

- (void)onFinalResponseReceived:(RecognitionResult *)result {
//    NSLog(@"%@", result.RecognizedPhrase)
//    bool isFinalDicationMessage = (result.RecognitionStatus == RecognitionStatus_EndOfDictation ||
//     result.RecognitionStatus == RecognitionStatus_DictationEndSilenceTimeout) || result.RecognitionStatus == RecognitionStatus_RecognitionSuccess;
//    
//    if (isFinalDicationMessage ) {
        self.isRecording = NO;
        [self.microphoneClient endMicAndRecognition];
//    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.recordButton.imageView setImage:[UIImage imageNamed:@"record"]];
    });
    
    dispatch_async(dispatch_get_main_queue(), ^{
    self.meLeftConstraint.constant = -150.0f;
    self.govRightConstraint.constant = 0.0f;
    [UIView animateWithDuration:2.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:nil];
    });
    
    // Figure out what the most confident phrase was and we will show it back to them
    RecognizedPhrase *mostConfidentPhrase = nil;
    
    for (RecognizedPhrase *p in result.RecognizedPhrase) {
        if (!mostConfidentPhrase) {
            mostConfidentPhrase = p;
        } else if (mostConfidentPhrase.Confidence > p.Confidence) {
            mostConfidentPhrase = p;
        }
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        self.loadingImage.hidden = NO;
        [UIView animateWithDuration:1.0f delay:0.0f options:UIViewAnimationOptionAutoreverse|UIViewAnimationOptionRepeat|UIViewAnimationOptionCurveEaseInOut animations:^{
            self.loadingImage.transform = CGAffineTransformMakeScale(0.5, 0.5);
        } completion:^(BOOL finished) {
            self.loadingImage.hidden = YES;
        }];
    });
    
    if (mostConfidentPhrase) {
        dispatch_async(dispatch_get_main_queue(), ^{
            self.spokenText = mostConfidentPhrase.DisplayText;
//            [self speakText:mostConfidentPhrase.DisplayText];
            
            [DataManager submitInputText:[APIInput inputWithText:mostConfidentPhrase.DisplayText facebookId:self.facebookId] completion:^(APIAnswer *answer, NSError *error) {
                [self speakText:answer.answer completion:^{
                    dispatch_async(dispatch_get_main_queue(), ^{
                        self.answerTextLabel.text = [NSString stringWithFormat:@"\"%@\"", answer.answer];
                        self.suggestionTextLabel.text = answer.suggestion;
                        
                        if (answer.topic && answer.topic.length > 0) {
                            self.dataSetInfoLabel.text = [NSString stringWithFormat:@"Data set: %@", answer.topic];
                        } else {
                            self.dataSetInfoLabel.text = @"";
                        }
                        
                        [UIView animateWithDuration:0.01
                                              delay:0.0
                                            options:(UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction)
                                         animations:^ {
                                             self.loadingImage.transform = CGAffineTransformIdentity;
                                         }
                                         completion:nil
                         ];
                        
                        [self.view layoutIfNeeded];
                        
                        self.meLeftConstraint.constant = -150.0f;
                        self.govRightConstraint.constant = -150.0f;
                        [UIView animateWithDuration:2.0
                                              delay:0.0
                                            options:UIViewAnimationOptionCurveEaseInOut
                                         animations:^{
                                             [self.view layoutIfNeeded];
                                         } completion:nil];
                    });
                }];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.spokenTextLabel.text = [NSString stringWithFormat:@"\"%@\"", self.spokenText];
                });
            }];
        });
    }
}

- (void)onError:(NSString *)errorMessage withErrorCode:(int)errorCode {
    NSString *error = ConvertSpeechErrorToString(errorCode);
}

#pragma mark - IBActions

- (IBAction)Record:(id)sender {
    [UIView animateWithDuration:0.4 delay:0.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.instructionLabel.alpha = 0.0f;
        
        self.userBubbe.alpha = 1.0f;
        self.answerBubble.alpha = 1.0f;
        self.suggestionBubble.alpha = 1.0f;
    } completion:nil];
    
    if (self.isRecording) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.recordButton.imageView setImage:[UIImage imageNamed:@"record"]];
        });
        
        self.isRecording = NO;
        [self.microphoneClient endMicAndRecognition];
        return;
    }
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.recordButton.imageView setImage:[UIImage imageNamed:@"record-red"]];
    });
    
    NSURL *url = [[NSBundle mainBundle] URLForResource:@"ding" withExtension:@"wav"];
    NSError *error;
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    [self.player play];
    
    self.meLeftConstraint.constant = 0.0f;
    [UIView animateWithDuration:2.0
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     } completion:nil];
    
    // Set the text to empty
    self.spokenText = @" ";
    self.spokenTextLabel.text = [NSString stringWithFormat:@"\"%@\"", self.spokenText];
    
    self.isRecording = YES;
    OSStatus status = [self.microphoneClient startMicAndRecognition];
    if (status) {
        NSString *error = ConvertSpeechErrorToString(status);
    }
}

NSString* ConvertSpeechErrorToString(int errorCode)
{
    switch ((SpeechClientStatus)errorCode) {
        case SpeechClientStatus_SecurityFailed:         return @"SpeechClientStatus_SecurityFailed";
        case SpeechClientStatus_LoginFailed:            return @"SpeechClientStatus_LoginFailed";
        case SpeechClientStatus_Timeout:                return @"SpeechClientStatus_Timeout";
        case SpeechClientStatus_ConnectionFailed:       return @"SpeechClientStatus_ConnectionFailed";
        case SpeechClientStatus_NameNotFound:           return @"SpeechClientStatus_NameNotFound";
        case SpeechClientStatus_InvalidService:         return @"SpeechClientStatus_InvalidService";
        case SpeechClientStatus_InvalidProxy:           return @"SpeechClientStatus_InvalidProxy";
        case SpeechClientStatus_BadResponse:            return @"SpeechClientStatus_BadResponse";
        case SpeechClientStatus_InternalError:          return @"SpeechClientStatus_InternalError";
        case SpeechClientStatus_AuthenticationError:    return @"SpeechClientStatus_AuthenticationError";
        case SpeechClientStatus_AuthenticationExpired:  return @"SpeechClientStatus_AuthenticationExpired";
        case SpeechClientStatus_LimitsExceeded:         return @"SpeechClientStatus_LimitsExceeded";
        case SpeechClientStatus_AudioOutputFailed:      return @"SpeechClientStatus_AudioOutputFailed";
        case SpeechClientStatus_MicrophoneInUse:        return @"SpeechClientStatus_MicrophoneInUse";
        case SpeechClientStatus_MicrophoneUnavailable:  return @"SpeechClientStatus_MicrophoneUnavailable";
        case SpeechClientStatus_MicrophoneStatusUnknown:return @"SpeechClientStatus_MicrophoneStatusUnknown";
        case SpeechClientStatus_InvalidArgument:        return @"SpeechClientStatus_InvalidArgument";
    }
    
    return [[NSString alloc] initWithFormat:@"Unknown error: %d\n", errorCode];
}
@end
