//
//  DataManager.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "APIUser.h"
#import "APIAnswer.h"
#import "APILocation.h"
#import "APIInput.h"

@interface DataManager : NSObject

+ (void)submitFacebookUser:(APIUser *)user completion:(void(^)(BOOL success, NSError *error))completion;
+ (void)submitInputText:(APIInput *)input completion:(void(^)(APIAnswer *answer, NSError *error))completion;

+ (void)submitLocation:(APILocation *)location;

+ (void)getAudioForText:(NSString *)text completion:(void(^)(NSData *data, NSError *error))completion;

@end
