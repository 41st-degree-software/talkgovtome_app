//
//  LocationManager.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "LocationManager.h"
#import "APILocation.h"
#import "DataManager.h"

@implementation LocationManager

static NSString *facebookId;
static CLLocation *location;

+ (void)setLocation:(CLLocation *)l {
    location = l;
    [LocationManager updateApiWithLocation];
}

+ (void)setFacebookId:(NSString *)fbId {
    facebookId =fbId;
    [LocationManager updateApiWithLocation];
}

+ (void)updateApiWithLocation {
    if (location != nil && facebookId != nil) {
        APILocation *l = [APILocation new];
        l.facebook_id = facebookId;
        l.latitude = [NSString stringWithFormat:@"%f", location.coordinate.latitude];
        l.longitude = [NSString stringWithFormat:@"%f", location.coordinate.longitude];
        
        [DataManager submitLocation:l];
        
    }
}

@end
