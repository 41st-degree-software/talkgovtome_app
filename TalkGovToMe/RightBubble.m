//
//  RightBubble.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "RightBubble.h"

@implementation RightBubble

- (void)awakeFromNib {
    self.backgroundColor = [UIColor clearColor];
}

- (void)drawRect:(CGRect)rect {
    //// General Declarations
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    //// Color Declarations
    UIColor* color = [UIColor colorWithRed: 0.517 green: 0.71 blue: 0.706 alpha: 1];
    
    CGRect newRect = CGRectMake(0, 0, CGRectGetWidth(self.bounds) - 7, CGRectGetHeight(self.bounds));
    //// Rectangle Drawing
    UIBezierPath* rectanglePath = [UIBezierPath bezierPathWithRect: newRect];
    [color setFill];
    [rectanglePath fill];
    
    
    //// Rectangle 2 Drawing
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, CGRectGetWidth(self.bounds) -7, -0.14);
    CGContextRotateCTM(context, 43.96 * M_PI / 180);
    
    UIBezierPath* rectangle2Path = [UIBezierPath bezierPathWithRect: CGRectMake(0, 0, 10, 10)];
    [color setFill];
    [rectangle2Path fill];
    
    CGContextRestoreGState(context);
    
    [super drawRect:rect];
}

@end
