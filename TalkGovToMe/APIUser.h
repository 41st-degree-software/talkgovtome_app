//
//  APIUser.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIUser : NSObject

@property (strong, nonatomic) NSString *first_name;
@property (strong, nonatomic) NSString *last_name;
@property (strong, nonatomic) NSString *facebook_id;
@property (strong, nonatomic) NSString *gender;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *hometown;
@property (strong, nonatomic) NSString *friends;
@property (strong, nonatomic) NSString *dob;

+ (APIUser *)userWithFacebookUser:(NSDictionary *)facebookUser;
- (NSDictionary *)dictionary;

@end
