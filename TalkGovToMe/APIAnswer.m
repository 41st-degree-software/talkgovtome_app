//
//  APIAnswer.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "APIAnswer.h"

@implementation APIAnswer

+ (APIAnswer *)answerFromDictionary:(NSDictionary *)dictionary {
    APIAnswer *answer = [APIAnswer new];
    
    [answer setValuesForKeysWithDictionary:dictionary];
    
    return answer;
}

@end
