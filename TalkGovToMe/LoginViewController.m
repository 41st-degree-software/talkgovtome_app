//
//  LoginViewController.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "LoginViewController.h"
#import "Segues.h"
#import "DataManager.h"
#import "LocationManager.h"
#import <AVFoundation/AVFoundation.h>

@interface LoginViewController()

@property (weak, nonatomic) IBOutlet FBSDKLoginButton *loginButton;
@property (strong, nonatomic) CLLocationManager *locationManager;

@property (strong, nonatomic) AVAudioPlayer *player;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    self.loginButton.readPermissions = @[@"public_profile", @"email", @"user_friends", @"user_birthday", @"user_hometown", @"user_location"];
    
    [FBSDKProfile enableUpdatesOnAccessTokenChange:YES];
    
    
    if ([FBSDKAccessToken currentAccessToken]) {
        [self performSegueWithIdentifier:[Segues FacebookLogin] sender:self];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self.navigationController setNavigationBarHidden:true animated:animated];
    
    [self prepareLocation];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [self.navigationController setNavigationBarHidden:false animated:true];
}

#pragma mark - Core Location

- (void)prepareLocation {
    if ([CLLocationManager authorizationStatus] == (kCLAuthorizationStatusRestricted | kCLAuthorizationStatusDenied)) {
        return;
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined) {
        [self.locationManager requestWhenInUseAuthorization];
    }
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    if (self.locationManager) {
        [self.locationManager startUpdatingLocation];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if ([locations count] > 0) {
        CLLocation *location = locations[0];
        [self.locationManager stopUpdatingLocation];
        [LocationManager setLocation:location];
    }
}

#pragma mark - FBSDKLoginButtonDelegate

- (void)loginButton:(FBSDKLoginButton *)loginButton didCompleteWithResult:(FBSDKLoginManagerLoginResult *)result error:(NSError *)error {
    if (error || result.isCancelled) return;
    [self performSegueWithIdentifier:[Segues FacebookLogin] sender:self];
}

- (void)loginButtonDidLogOut:(FBSDKLoginButton *)loginButton {
    
}

@end
