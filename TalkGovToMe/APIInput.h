//
//  APIInput.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 5/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIInput : NSObject

@property (strong, nonatomic) NSString *input;
@property (strong, nonatomic) NSString *facebook_id;

+ (APIInput *)inputWithText:(NSString *)text facebookId:(NSString *)facebookId;
- (NSDictionary *)dictionary;

@end
