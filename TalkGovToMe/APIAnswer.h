//
//  APIAnswer.h
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APIAnswer : NSObject

@property (strong, nonatomic) NSString *answer;
@property (strong, nonatomic) NSString *suggestion;
@property (strong, nonatomic) NSString *topic;

+ (APIAnswer *)answerFromDictionary:(NSDictionary *)dictionary;

@end
