//
//  APIUser.m
//  TalkGovToMe
//
//  Created by Evan Robertson on 4/07/2015.
//  Copyright (c) 2015 41stDegree. All rights reserved.
//

#import "APIUser.h"

@implementation APIUser

+ (APIUser *)userWithFacebookUser:(NSDictionary *)facebookUser {
    APIUser *user = [APIUser new];
    
    user.facebook_id = facebookUser[@"id"];
    user.first_name = facebookUser[@"first_name"];
    user.last_name = facebookUser[@"last_name"];
    user.dob = facebookUser[@"birthday"];
    user.gender = facebookUser[@"gender"];
    
    NSDictionary *hometown = facebookUser[@"hometown"];
    user.hometown = hometown[@"name"];
    
    return user;
}

- (NSDictionary *)dictionary {
    return [self dictionaryWithValuesForKeys:@[@"first_name", @"last_name", @"facebook_id", @"dob", @"gender", @"hometown", @"friends"]];
}

@end
